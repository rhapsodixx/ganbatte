<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<title>Mozaiku</title>

	<!-- bootstrap -->
	<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />

	<!-- custom -->
	<link href="{{ asset('assets/css/main.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" />
	<link href="{{ asset('lightbox/css/lightbox.css') }}" rel="stylesheet" />

	<style>
		body{
			font-family: "Ubuntu", sans-serif;
			font-size: 80px;
		}

		.container-full {
		  	margin: 0 auto;
		  	width: 100%;
		}
		img.grayscale {
			filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale"); /* Firefox 3.5+ */
			filter: gray; /* IE6-9 */
			-webkit-filter: grayscale(100%); /* Chrome 19+ & Safari 6+ */
			opacity: 0.2;
		}

		img.grayscale:hover {
		  	filter: none;
		  	-webkit-filter: grayscale(0%);
		  	opacity: 0.6;
		}

		.mosaicflow__column 
		{ 
			float:left; 
		} 
		.mosaicflow__item img 
		{ 
			display:block; 
			width:100%; 
			height:auto; 
		}

		#grid_container
		{
		    width: 100%;
		    height: 100%;
		    position: absolute;
		    top: 0;
		    left: 0;
		}

		.grid_info {
		    width: 75%;
			height: 60%;
			overflow: auto;
			margin: auto;
			position: absolute;
			top: 0; left: 0; bottom: 0; right: 0;
		}

		#grid_container {
		    z-index: 1;
		}

	</style>
</head>
<body class="background" >
	<div class="container-full">

		<div class="grid_info">
			<p id="text"><span class="texts">semangat</span> isty !</p>
		</div>

		<!-- holder -->
		<div id="grid_container" class="clearfix"> 

			@for ($i = 1; $i < 68; $i++)
				<div class="mosaicflow__item"> 
					<a href="assets/img/{{ $i }}.jpg" data-lightbox="beach">
						<img class="grayscale" src="assets/img/{{ $i }}.jpg" alt="">
					</a>
				</div> 
			@endfor
			
		</div>

		<!-- Load JavaScript -->
		<script src="{{ asset('lightbox/js/jquery-1.11.0.min.js') }}"></script>
		<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/js/jquery.mosaicflow.min.js') }}"></script>
		<script src="{{ asset('lightbox/js/lightbox.min.js') }}"></script>
		<script src="{{ asset('assets/js/jquery.fittext.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/js/jquery.lettering.js') }}" type="text/javascript"></script>
		<script src="{{ asset('assets/js/jquery.textillate.js') }}" type="text/javascript"></script>
		<script>

			var counter = 0;
			var sentences 	 = [ "semangat",
							     "がんばって",
								 "Allez",
							     "화이팅",
							     "加油" ];
			var color 	 = [ "red",
							     "blue",
								 "green",
							     "cyan",
							     "orange" ];

			function fadeOut(selector)
			{
				$(selector).fadeOut(2000,function(){
					counter++;
					if( counter == sentences.length )
					{
						counter = 0;
					}
					$(selector).text(sentences[counter]);
					$(selector).css('color', color[counter]);
					fadeIn(selector);
				});
			}

			function fadeIn(selector)
			{
				$(selector).fadeIn(2000,fadeOut(selector));
			}

			$(function()
			{	
				var $container = $('#grid_container');
				
				$('#grid_container').mosaicflow({
				    minItemWidth: 150
				});

				fadeOut('.texts');

				// $('.texts').textillate({ 
				// 	loop: true,
				// 	initialDelay: 0,
				// 	minDisplayTime: 1000,
				// 	in: { effect: 'fadeIn', sync: true },
				// 	out: { effect: 'fadeOut', sync: true, callback:function(){

				// 	} },
				// });

			});

		</script>

	</div>
</body>